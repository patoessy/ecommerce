import Service from "./Service";
const resource = "/auth";
export default {
  login(userCredentials) {
    return Service.post(`${resource}`, userCredentials, {
      headers: {
        "Content-Type": "application/json"
      }
    });
  }
};
