import axios from "axios";
const baseDomain = "http://localhost:8080";
const baseURL = `${baseDomain}` + "/api";
//axios.defaults.headers.common["Authorization"] = `Bearer ${localStorage.getItem(
//"token"
//)}`;
let instance = axios.create({
  baseURL,
  Headers: {
    headers: {
      "Content-Type": "application/json"
    }
  },
  transformRequest: function(data) {
    return JSON.stringify(data);
  },
  params: {}
});
instance.interceptors.request.use(
  config => {
    const token = localStorage.getItem("token");
    //alert("request token is: " + token);
    config.headers["Authorization"] = "Bearer " + token;
    return config;
  },
  error => {
    Promise.reject(error);
  }
);

instance.interceptors.response.use(
  response => {
    return response;
  },
  function(error) {
    const originalRequest = error.config;
    if (error.response.status !== 401) {
      return new Promise((resolve, reject) => {
        reject(error);
      });
    }
    if (
      error.response.status === 401 &&
      originalRequest.url === "http://localhost:8081/api/auth"
    ) {
      this.$router.push("/login");
      return Promise.reject(error);
    }
    if (error.response.status === 401 && !originalRequest._retry) {
      originalRequest._retry = true;
      const user = JSON.parse(localStorage.getItem("user"));
      //alert("new token");
      let userData = { username: user.email, password: user.password };
      return axios.post(`${baseURL}` + "/auth", userData).then(result => {
        //alert("Token: " + result.data.token);
        localStorage.setItem("token", result.data.token);
        axios.defaults.headers.common["Authorization"] =
          "Bearer " + localStorage.getItem("token");
        return axios(originalRequest);
      });
    }
    return Promise.reject(error);
  }
);

export default instance;
