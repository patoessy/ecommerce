import Service from "./Service";
const resource = "/order";

export default {
  get(customerId) {
    return Service.get(`${resource}/${customerId}`);
  },
  edit(order) {
    return Service.put(`${resource}`, order);
  },
  checkout(customerId) {
    return Service.post(`${resource}/${customerId}`);
  }
};
