import CustomerService from "./CustomerService";
import ProductService from "./ProductService";
import CartService from "./CartService";
import AuthService from "./AuthService";
import OrderService from "./OrderService";

const services = {
  customers: CustomerService,
  products: ProductService,
  cart: CartService,
  auth: AuthService,
  order: OrderService
};

export const ServiceFactory = {
  get: name => services[name]
};
