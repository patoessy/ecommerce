import Service from "./Service";
const resource = "/cart";

export default {
  get(customerId) {
    return Service.get(`${resource}` + "/customer/" + `${customerId}`);
  },
  addToCart(cart) {
    return Service.post(`${resource}`, cart, {
      headers: {
        "Content-Type": "application/json"
      }
    });
  },
  updateCart(cart) {
    return Service.put(`${resource}`, cart);
  },
  delete(cartId) {
    return Service.delete(`${resource}/${cartId}`);
  }
};
