import Service from "./Service";
const resource = "/products";

export default {
  get() {
    return Service.get(`${resource}`);
  },
  getProduct(productId) {
    return Service.get(`${resource}/${productId}`);
  },
  createProduct(product) {
    return Service.post(`${resource}`, product);
  }
};
