import Service from "./Service";
const resource = "/users/customer";
export default {
  get() {
    return Service.get(`${resource}`);
  },
  getUser(userId) {
    return Service.get(`${resource}/${userId}`);
  },
  createUser(user) {
    alert("Creating user");
    return Service.post(`${resource}`, user, {
      headers: {
        "Content-Type": "application/json"
      }
    });
  },
  login(email, pass) {
    return Service.get(`${resource}` + "/auth" + `/${email}/${pass}`);
  }
};
