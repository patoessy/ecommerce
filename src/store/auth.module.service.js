import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);
export default new Vuex.Store({
  state: {
    loggedIn: false
  },
  mutations: {
    logout(state) {
      state.loggedIn = false;
    },
    login(state) {
      state.loggedIn = true;
    }
  },
  getters: {
    loggedIn: state => !!state.logged
  },
  actions: {
    logout({ commit }) {
      commit("logout");
      localStorage.removeItem("user");
      this.$router.push("/");
    },
    login({ commit }) {
      commit("login");
    }
  }
});
