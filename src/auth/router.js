import Vue from "vue";
import VueRouter from "vue-router";
import Products from "../components/Product.vue";
import Cart from "../views/Cart";
import Order from "../views/Order";
import Store from "../store/auth.module.service";
        Vue.use(VueRouter);
        const routes = [
        {
        path: "/",
                name: "home",
                component: Products
        },
        {
        path: "/order",
                name: "order",
                component: Order,
                meta: {
                requiresAuth: true
                }
        },
        {
        path: "/cart",
                name: "cart",
                component: Cart,
                meta: {
                requiresAuth: true
                }
        },
        {
        path: "/signup",
                name: "signup",
                component: () => import(/* webpackChunkName: "/signup" */ "../views/Signup.vue"),
                meta: {
                guest: true
                }
        },
        {
        path: "/login",
                name: "login",
                component: () =>
        import(/* webpackChunkName: "/login" */ "../views/Login.vue"),
                meta: {
                guest: true
                }
        }
        ];
        const router = new VueRouter({
        mode: "history",
                routes
        });
        router.beforeEach((to, from, next) => {
        /**
         * First look for user data in local storage incase they chose keep me signed in
         */
        if (to.matched.some(record => record.meta.requiresAuth)) {
        if (localStorage.getItem("user") == null) {
        alert("User is null");
                next({ path: "login", params: { nextUrl: to.fullPath } });
        } else {
        Store.dispatch("login");
                let user = JSON.parse(localStorage.getItem("user"));
                //alert(JSON.parse(user).date);
                //console.dir(user);
                if (to.matched.some(record => record.meta.is_admin)) {
        if (user.type == "ADMIN") {
        next({ name: "admin" });
        } else if (user.type == "CUSTOMER") {
        next({ name: "cart" });
        }
        } else {
        next();
        }
        }
        } else if (to.matched.some(record => record.meta.guest)) {
        if (localStorage.getItem("user") == null) {
        next();
        } else {
        next({ name: "cart" });
        }
        } else {
        next();
        }
        });
export default router;
